# Removes tandem interpretations, comments, and bar numbers from
# all files in the current directory. All files in the cd must be in
# humdrum **kern format.

mkdir -p ../stripped
for f in *; do
	grep -v -E "^[!|*]" $f | sed 's/\(==*\)[0-9][0-9]*/\1/g' > ../stripped/$f
done
