# Processes generated music output to convert back to valid humdrum **kern
#     - adds header assuming string quartet and blank key sig
#     - numbers the bars
#     - closes the file properly

import sys

if len(sys.argv) != 3:
    print("Usage: python3 postprocess.py [file_in] [file_out]")
    sys.exit()

fname = sys.argv[1]
fout = sys.argv[2]
f = open(fname,"r").readlines()
r = []

r.append("!!! Opus 1 No. 10 - ai-composer - haydn\n")
r.append("**kern\t**kern\t**kern\t**kern\n")
r.append("*staff4\t*staff3\t*staff2\t*staff1\n")
r.append("*Icello\t*Iviola\t*Iviolin\t*Iviolin\n")
r.append("*k[]\t*k[]\t*k[]\t*k[]\n")
r.append("*clefF4\t*clefC3\t*clefF\t*clefF\n")

bar = 2
for l in f:
    if l.startswith("="):
        if not l.startswith("=="):
            r.append("={bar}\t={bar}\t={bar}\t={bar}\n".format(bar=bar))
            bar += 1
    else:
        r.append(l)

r.append("==\t==\t==\t==\n")
r.append("*-\t*-\t*-\t*-\n")

open(fout,"w").writelines(r)
