# Takes a **kern file in any key as input and transposes it to the key of C
# using the awk tool trans.

import os
import sys
import re

if len(sys.argv) != 2:
    print "Usage: transpose_to_c.py FILENAME"
    sys.exit()

filename = sys.argv[1]
d = ''
c = ''
key_sig_count = 0

for line in open(filename):
    line = line.strip().split()
    m = re.match("\*k\[(([a-g](#|-))*)\]", line[0])
    if m:
        key_sig_count += 1
        if key_sig_count > 1:
            print "More than one key signature in this file:", filename
            sys.exit()
        notes = m.group(1)
        if len(notes) == 0:
            #key is c major / a minor
            d = '+0'
            c = '+0'
        elif notes[1] == '#':
            notes = notes.split('#')[:-1]
            if len(notes) == 1:
                # g major
                d = '+3'
                c = '+5'
            elif len(notes) == 2:
                # d major
                d = '-1'
                c = '-2'
            elif len(notes) == 3:
                # a major
                d = '+2'
                c = '+3'
            elif len(notes) == 4:
                # e major
                d = '-2'
                c = '-4'
            elif len(notes) == 5:
                # b major
                d = '+1'
                c = '+1'
            elif len(notes) == 6:
                # f# major
                d = '-6'
                c = '-3'
            elif len(notes) == 7:
                # c# major
                d = '+0'
                c = '-1'
        elif notes[1] == '-':
            notes = notes.split('-')[:-1]
            if len(notes) == 1:
                # f major
                d = '-3'
                c = '-5'
            elif len(notes) == 2:
                # b- major
                d = '+1'
                c = '+2'
            elif len(notes) == 3:
                # e- major
                d = '-2'
                c = '-3'
            elif len(notes) == 4:
                # a- major
                d = '+2'
                c = '+4'
            elif len(notes) == 5:
                # d- major
                d = '-1'
                c = '-1'
            elif len(notes) == 6:
                # g- major
                d = '+3'
                c = '+6'
            elif len(notes) == 7:
                # c- major
                d = '+0'
                c = '+1'
        else:
            print "Should be # or - but got", notes[1]
if key_sig_count == 0:
    print "No key signature in file:", filename
    sys.exit()
command = "../tools/trans -d {} -c {} -k '*k[]' {} > tr/{}.tr".format(d, c, filename, os.path.basename(filename))
os.system(command)
