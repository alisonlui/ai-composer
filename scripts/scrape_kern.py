# Scrapes kern.humdrum.org for kern files based on a keyword search
# -*- coding: utf-8 -*-

import bs4 , requests,os, datetime, glob, os


# create the output folder with a timestamp
mydir = os.path.join(os.getcwd(),("output_" + datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')))
os.makedirs(mydir)


# Get all the links from "Haydn" search
res1 = requests.get("http://kern.humdrum.org/search?s=t&keyword=string+quartet&type=Text")
soup = bs4.BeautifulSoup(res1.text,"html.parser")

#For each link found, return the HTML part which contains a link for that specific song info and its .krn file
for link in soup.select('[href*=format="info"]'):
    #print(link.getText(), link['href'])
    #print("---")
    
    res = requests.get(link['href'])
    soup = bs4.BeautifulSoup(res.text,"html.parser")
    
    
    #For each link found (which contains the .krn file for that song), gets the 
    for link in soup.select('[href]'):
        
        if (".krn" in link.getText()):    
            
            #request the file
            res2 = requests.get(link['href'])
            response = requests.get(link['href'], auth=('user', 'pass'))
            
            # get html
            html_bytes = response.content

            # turn bytes into string
            html = html_bytes.decode("utf-8",'ignore')
        
            # write data into a file
            if ("!!!COM: Haydn, Franz Joseph" is not html):
                text_file2 = open(mydir + "/" + link.getText(), "w")
                text_file2.write(html)
                text_file2.close()       

# -------------------------------------------------------------------------------- 
# 2nd part of the code

filenames = []

os.chdir(mydir)

for file in glob.glob("*.krn"):
    filenames.append(file)


for fname in filenames:
    if '!!!COM: Haydn, Franz Joseph' in open(fname).read():
        os.remove(fname)
        print("file removed")
        
    
# print the total number of lines of all .krns files    
original_number_of_lines=0
for fname in filenames:
    with open(fname) as infile:
        for line in infile:
            original_number_of_lines = original_number_of_lines + sum(1 for _ in infile)
            
print("number of lines on the original .krns files: " + str(original_number_of_lines))


# combine all the different .krns in 1 file
with open(mydir + '/total.txt', 'w') as outfile:
    for fname in filenames:
        with open(fname) as infile:
            for line in infile:
                outfile.write(line)

#Get the unique file (which contains all the lines)                
f = open(mydir + '/total.txt',"r")

#Next, get all lines from the file:
lines = f.readlines()

#Now you can close the file:
f.close()

#And reopen it in write mode:
f = open(mydir + '/total.txt',"w")

#Then, write your lines back, except the line you want to delete
for line in lines:
    if ("!!!EMD: Manual" in line or "!!!" not in line):
        if("!!!EMD: Manual" in line):
            f.write("-------"+"\n")
        else:
            f.write(line)

#At the end, close the file again.
f.close()


final_number_of_lines=0
with open(mydir + '/total.txt') as infile:
    for line in infile:
        final_number_of_lines = final_number_of_lines + sum(1 for _ in infile)
print("number of lines on the final file: " + str(final_number_of_lines))
            
            
