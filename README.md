# AI Composer
## Requirements
Download [Andrej Karpathy's LSTM](https://github.com/karpathy/char-rnn). In the `char-rnn/data` folder, create a directory and inside that directory place the training data file, and name that file `input.txt`. Then follow Karpathy's instructions to train the model. Sample command line to train on CPU:
```
th train.lua -data_dir data/transposed -rnn_size 128   
             -num_layers 3 -dropout 0.3 
             -checkpoint_dir cv/transposed -gpuid -1
```
Prepared training data files that are comlpetely ready to train on are provided in the `data` directory. See scripts and tools for processing data yourself.

## Scripts
This directory contains the main scripts that were developed to scrape and process the data.
* `scrape_kern.py`: The scraper used to obtain the data from [http://kern.humdrum.org/](http://kern.humdrum.org/).
* `postprocess.py`: Use this script to convert generated output from the model back to valid Humdrum \*\*kern format. Adds an arbitrary title in line 17 which you may want to change. Not guaranteed to be valid \*\*kern after postprocessing because the model is prone to rhythmic errors which must be corrected manually.
* `strip_metadata.sh`: Part of the preprocessing step. Copy this script into a directory with original \*\*kern files and run to remove comments, tandem interpretations, and bar numbers from all files. You may also want to transpose as a preprocessing step; if so, this must be done before stripping metadata. Removing extra columns is also required as a preprocessing step but must be done manually since different files may have different numbers of different types of columns and it is up to you which columns should be kept (Sometimes a single instrument has more than one staff because some measures have multiple notes for that instrument. In this case it is recommended to remove the staff that is mostly rests to keep the number of spines at 4 throughout the training data). Removing extra columns should be done after stripping metadata.
* `transpose_to_c.py`: Pass a \*\*kern file in as a command line argument to transpose it to the key of C regardless of its original key. Depends on `tools/trans`.

## Tools
This directory contains Humdrum tools from [hum2xml](http://extras.humdrum.org/man/hum2xml/) and [trans](https://csml.som.ohio-state.edu/Humdrum/commands/trans.html). However, they have been modified and debugged so be sure to use the versions included here and do not download fresh copies.
* `hum2xml` converts a \*\*kern file to musicXML
* `trans` transposes a \*\*kern file by a specified number of semitones and diatonic letter names

## Data
The data folder contains the preprocessed transposed and nontranspsed training data exactly as it was used in our experiments.

The original data can be found at: [Google Drive Folder](https://drive.google.com/drive/folders/1Kc5I16nvaniBbtQoLebwH84l9xGW91Tz?usp=sharing)

The Google Drive folder contains the original Haydn string quartet \*\*kern files as well as all the additional string quartet \*\*kern files from other composers used in the evaluation as mentioned in the project report. 

## Example Generated Music
The `generated_music` directory contains raw generated \*\*kern files which we obtained from our three trained models. Headers, footers, and bar numbers have been added in post-processing, but rythmic errors have not been corrected so they may not be immediately interpretable by \*\*kern tools.

Op1 is from nontransposed training data, op2 from transposed training data, and op3 from the larger model trained on transposed training data.

The Google Drive folder contains a subset of these pieces where rythmic errors have been corrected. An audio file is provided for one piece, and the other pieces may be listened to quickly (without converting to musicXML for the high quality synthetic piano) by copy and pasting their contents into this [online \*\*kern viewer](https://verovio.humdrum.org/).
